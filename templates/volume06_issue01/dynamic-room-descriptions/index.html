{% extends "issue/article/index.html" %}
{% block page_title %}
	Dynamic Room Descriptions
{% endblock %}
{% block article_authors %}<span class="author-nick">Janua</span>{% endblock %}
{% block article_date %}8th February 2014{% endblock %}
{% block article_content %}
  <p class="c1 c8"><span>I&#8217;ve always wondered, &#8220;What&#8217;s the point of day and night, seasons, weather,
  and other cycles in a MUD?&#8221; Sure, I see the messages when the sun sets and rises. Maybe there are weather
  messages if your MUD has those. Maybe your character will be affected by those things, if of a race or class that is
  sensitive to them. Shouldn&#8217;t the way the world looks change in response to those, and other events as
  well?</span></p>

  <p class="c6 c1"></p>

  <p class="c1 c8"><span>I imagine most builders like the idea, but most would feel intimidated by how much work that
  would add. Most implementations rely on separate or appended descriptions for day, night, and seasons, or complex
  tagging that is difficult to understand and remember. That&#8217;s a huge burden on builders and it&#8217;s very
  inflexible. We&#8217;ve come up with a tagging system, similar to HTML or XML, that allows builders to change as
  little as a word or a phrase, and is very intuitive and easy to remember.</span></p>

  <h3>Seasons, time of day, and phases of the moon</h3>

  <p class="c1 c8"><span>Most MUDs already have day/night cycles, seasons, and moon phases. Implementing these tags is
  relatively easy for the developer. Simple tags make it easy for the builder, too.</span></p>

  <p class="c1 c6"></p>

  <p class="c1 c8"><span>Let&#39;s look at an example. The following room has phrases that change depending on the
  season. Notice that I wrote this so that only part of the sentence changes:</span></p>

<p class="code">
[ 1] &#160;The cobbled road runs up to the city&#39;s thick wall, and a wide, arched<br/>
[ 2] &#160;opening allows passage to the countryside west of town.<br/>
[ 3] &#160;The road continues beyond the open portcullis, making its way across a rocky field<br/>
[ 4] &#160;&lt;winter&gt;filled with patches of crusty snow.&lt;/winter&gt;<br/>
[ 5] &#160;&lt;spring&gt;dotted with hints of sprouting greenery.&lt;/spring&gt;<br/>
[ 6] &#160;&lt;summer&gt;with clumps of green and yellow grass.&lt;/summer&gt;<br/>
[ 7] &#160;&lt;autumn&gt;filled with clumps of browning grass.&lt;/autumn&gt;<br/>
</p>

  <h3>Exit states and nested tags</h2>

  <p class="c1 c8"><span>Exits are another easy tag to implement because almost all MUDs have them. Don&#39;t limit
  yourself to doorways, either. You could write a room script to close an exit for something like a rockslide, and
  allow the player to clear the path and open the exit. The room description would reflect this change in
  state.</span></p>

  <p class="c6 c1"></p>

  <p class="c1"><span>Building on our example above, let&#39;s add a portcullis that players can open and
  close:</span></p>

  <p class="c6 c1"></p>

<p class="code">
[ 1] &#160;The cobbled road runs up to the city&#39;s thick wall, and a wide, arched<br/>
[ 2] &#160;opening allows passage to the countryside west of town.<br/>
[ 3] &#160;&lt;open=west&gt;<br/>
[ 4] &#160; &#160;Beyond the open portcullis, the road makes its way across a rocky field<br/>
[ 5] &#160; &#160;&lt;winter&gt;filled with patches of crusty snow.&lt;/winter&gt;<br/>
[ 6] &#160; &#160;&lt;spring&gt;dotted with hints of sprouting greenery.&lt;/spring&gt;<br/>
[ 7] &#160; &#160;&lt;summer&gt;with clumps of green and yellow grass.&lt;/summer&gt;<br/>
[ 8] &#160; &#160;&lt;autumn&gt;filled with clumps of browning grass.&lt;/autumn&gt;<br/>
[ 9] &#160;&lt;/open=west&gt;<br/>
[10] &#160;&lt;closed=west&gt;The opening is blocked by a lowered portcullis.&lt;/closed=west&gt;<br/>
</p>

  <p class="c1 c8"><span>You can see that the seasonal tags are nested within the <span class="code">&lt;open=west&gt;</span> tag. You&#39;ll
  want to allow unlimited nesting so your builders can do things like the example above.</span></p>

  <h3>Weather, &#8220;not&#8221; tags, and combining tags</h3>

  <p class="c1 c8"><span>Rather than invest time in a complicated weather back-end, we decided that weather would be
  very simple. The area has a setting for the chance of precipitation, and the MUD periodically changes the weather in
  each area based on this chance. That&#8217;s all our weather code has to do, because the dynamic tags take care of
  how weather looks in a room. Weather is tailored specifically for each area and room without complicated code in the
  back end. Builders can also choose to send area-wide messages using room scripts, adding to the ambiance in their
  area.</span></p>

  <p class="c1"><span>You might write something like this to reflect weather in a room:</span></p>

<p class="code">
[ 1] &#160;&lt;stormy&gt;Dark clouds fill the sky and<br/>
[ 2] &#160; &#160;&lt;winter&gt;the snow is so thick you can&#39;t see more than a few feet in front of your cold nose.&lt;/winter&gt;<br/>
[ 3] &#160; &#160;&lt;spring|summer&gt;light showers sprinkle across the mountain slopes.&lt;/spring|summer&gt;<br/>
[ 4] &#160; &#160;&lt;autumn&gt;snowflakes drift and dance in the crisp autumn air.&lt;/autumn&gt;<br/>
[ 5] &#160;&lt;/stormy&gt;<br/>
[ 6] &#160;&lt;!stormy&gt;The sky is clear and the mountain air is crisp and clean.&lt;/!stormy&gt;<br/>
</p>

  <p class="c1 c8"><span>Notice that I combined the spring and summer tags using a pipe character. This helps to
  simplify room descriptions and make them easier for builders to read and maintain.</span></p>

  <p class="c1 c8"><span>You&#8217;ll also notice that there is a <span class="code">stormy</span> tag and a <span class="code">!stormy</span> tag. We implemented
  &#8220;not&#8221; tags for all tags that made sense for it, such as day, night, and seasons.</span></p>

  <h3>Room flags</h3>

  <p class="c1 c8"><span>In addition to the tags for world or room conditions, we also implemented tags based on the
  state of room flags. These flags can be set or unset using room, mob, or object scripts. This allows a builder to
  create a unique condition that is displayed in the room description.</span></p>

  <p class="c1 c8"><span>Here&#39;s my original example with a lever that players can pull to open the gate:</span></p>

  <p class="code">
[ 1] &#160;The cobbled road runs up to the city&#39;s thick wall, and a wide, arched<br/>
[ 2] &#160;opening allows passage to the countryside west of town.<br/>
[ 3] &#160;&lt;open=west&gt;<br/>
[ 4] &#160; &#160;Beyond the open portcullis, the road makes its way across a rocky field<br/>
[ 5] &#160; &#160;&lt;winter&gt;rocky, barren field with patches of crusty snow.&lt;/winter&gt;<br/>
[ 6] &#160; &#160;&lt;spring&gt;rocky field dotted with hints of sprouting greenery.&lt;/spring&gt;<br/>
[ 7] &#160; &#160;&lt;summer&gt;rocky field with clumps of green and yellow grass.&lt;/summer&gt;<br/>
[ 8] &#160; &#160;&lt;autumn&gt;field filled with rocks and clumps of browning grass.&lt;/autumn&gt;<br/>
[ 9] &#160;&lt;/open=west&gt;<br/>
[10] &#160;&lt;closed=west&gt;The opening is blocked by a lowered portcullis.&lt;/closed=west&gt;<br/>
[11] &#160;A lever mounted on the wall next to the opening is<br/>
[12] &#160;&lt;flag=0&gt;up&lt;/flag=0&gt;<br/>
[13] &#160;&lt;flag=1&gt;down&lt;/flag=1&gt;.</p>

  <p class="c1 c8"><span>The room has a script that is executed when the player types</span> <span class="command">pull
  lever</span><span>. This script opens the door to the west and sets a flag on the room. Each room in our MUD has two
  types of these flags: one type is automatically unset when the room is reset, the other type is saved across resets
  (and reboots). We have three flags of each type in every room, allowing builders to create complex puzzles and other
  constructions.</span></p>

  <h3>Auto-formatting descriptions</h3>

  <p class="c1 c8"><span>One consequence of using tags is that you must automatically format room descriptions on the
  server. This means that if you allow multi-paragraph room descriptions, you will need to implement a special code for
  carriage returns. It also means that builders can write room descriptions in a way that is much easier to maintain.
  You&#39;ll notice that my examples break out the tags onto separate lines, which makes it easy to read and easy to
  edit. Changing a sentence is much faster. This is also an opportunity to remove the archaic double-space after the
  end of a sentence that some builders use.</span></p>

  <h3>Complimentary commands and functions</h3>

  <p class="c1 c8"><span>In order to review rooms with dynamic descriptions, especially when they are heavily used, we
  had to implement a look command that accepted a parameter. To use it, you enter</span> <span class="command">look &lt;tag
  name&gt;</span><span>&#160;where the tag name is exactly as it appears in the tag. For example, in the example room
  I&#39;ve provided, you could type</span> <span class="command">look open=west</span><span>&#160;and you would see what
  the room looks like to a player when the portcullis is open. Nested and combined tags are handled properly. You can
  type</span> <span class="code">look &lt;tag1&gt; &lt;tag2&gt;</span><span>&#160;to see the room when both conditions
  are true.</span></p>

  <p class="c6 c1"></p>

  <p class="c1 c8"><span>You&#8217;ll also want to provide functions for your scripting system that allow a builder to
  create room, mob, or object scripts that can influence rooms, as appropriate. The possibilities there are literally
  endless.</span></p>

  <h3>Considerations</h3>

  <p class="c2 c1"><span>My experience with this type of system is that it helps to provide examples for your builders
  of what you want rooms to look like, and how extensively you want rooms to be tagged. Some builders will avoid them
  entirely, and others will go completely nuts. I would recommend that you encourage builders to combine partial
  sentences like I did in the examples, and only change phrases or words.</span></p>

  <p class="c6 c1 c13"></p>

  <p class="c2 c1"><span>It&#8217;s also helpful to provide templates that your builders can quickly insert into room
  descriptions. If you have a web-based building tool, you could customize your text editor to include the
  tags.</span></p>

  <p class="c2 c6 c1"></p>

  <p class="c2 c1"><span>I would love to hear what you think or how you implemented this on your MUD.</span></p>
{% endblock %}
{% block article_bio_content %}
	Janua is a professional technical writer who has been building and developing on one MUD or another since the late 90s. You can reach her at <a href="mailto:janua.tld@gmail.com">janua.tld@gmail.com</a>.
{% endblock %}
