{% extends "issue/article/index.html" %}
{% block page_title %}
	A text MUD with a working ecology system
{% endblock %}
{% block article_authors %}<span class="author-nick">Molly O'Hara</span>{% endblock %}
{% block article_date %}30th May 2015{% endblock %}
{% block article_content %}
    <p>
      I’ve always liked consistency in the game world, which is actually a rare thing in most text MUDs.
    </p>
    <p>
      Many builders typically work on the same MUD, all of them have their own ideas, preferences and writing styles, and as a Head Builder you don’t want to hamper their imagination too much, for fear that they’ll just up and leave — which many of them do anyway. Consequently, many MUDs end up with a patchwork of rather incompatible zones, on a wide variety of themes and environments, where you could walk directly from an elven forest into a Smurf village, or from a desert landscape into a field of snow, without any logical distance or explanation.
    </p>
    <p>
      The other end of the spectrum, the grid based world, usually gets rather dull and monotonous, with very repetitive descriptions.
    </p>
    <p>
      As a new and totally inexperienced builder in a brand new MUD, almost twenty years ago, I was happy to be allowed to develop my own ideas, and so were all the other builders. There was a lot of enthusiasm among us, as we toiled away on the zones on the build port, while the coders had their own fun on the game port. Building is slow work, but with all that unbridled creativity, zones were produced at an amazingly fast pace. The result was… a patchwork, as it was in most other MUDs back then.
    </p>
    <p>
      As a new and totally inexperienced Head Builder in the nineties, I spent most of my time building “travelling” zones between all those different themed patches, so as to at least keep them apart a bit and provide some kind of explanation as to why they were so different. The result was better, but still not really spectacular. The MUD was still a patchwork, but by now with an amazingly large world to explore … and it kept growing.
    </p>
    <p>
      About ten years ago, I started to get ideas about a more interactive world, where the inhabitants — the “mobs” — would have lives of their own. This, in turn, would affect the players to a point where they had to think a bit about the choices they made before going on a killing spree. I ran with some of these ideas, and they resulted in lots of scripted mobs who responded to approaches by telling the players about their woes and sending them on various quests to help.
    </p>
    <p>
      The quests and the “intelligent” scripted mobs are now large and vital parts of our MUD, but it still isn’t quite the vision I had 10 years ago.
    </p>

    <h3>A cohesive MUD world with a working ecology</h3>
    <p>
      If I were to start a MUD from scratch today, with the knowledge I've gained over the years, I’d go about it in a very different way. I’d create a dynamic world that changes with the time of day and year, the weather and the seasons, with a working ecology system, where all plants and animals have a life cycle of their own, and where players also could affect the ecological balance with their actions — for better or worse.
    </p>
    <p>
      The players should be able to utilize all natural assets – minerals, plants and animals – and also cultivate the soil, to increase the crops. They should be able to hunt and kill the wild animals for their meat and hides, but also to domesticise herds of cattle, horses and sheep, fence them in for protection and drive them to the market to be sold. They should be able to develop most of the raw materials into more advanced and usable products by crafting. There should be a market where they could trade their products, and get a higher price the more developed they were. Above all there should be a balanced ecology system, so that the MUD would be alive and changing in yearly cycles, even if not a single player were logged on to it.
    </p>
    <p>
      I don’t know if such a MUD already exists somewhere. I’ve never come across one myself. Yet almost all the elements needed already exist in my own home MUD, which is run on modified Circle code and the DG_scripts. If anyone knows of something like this already existing, I’d appreciate a link, so I could visit and watch for myself. Otherwise, this is how I’d go about it, if I were starting from scratch today:
    </p>

    <h3>The world</h3>
    <p>
      I’d go for a combination between a large, continuous grid and “normal zones”. The main world would be grid based, and I’d definitely utilize the beautiful graphic maps from the Client app that KaVir developed for MUSHclient and so generously shared with the community.
    </p>
    <p>
      From this main grid you’d reach some “special zones”, which would be more like traditional MUD zones, such as islands, caverns, mine tunnels, or player built mansions, fortresses and cities. These extra zones would only show up on the main grid as a single entrance square, and you’d need to either enter a portal, or descend/climb some object to reach them.
    </p>
    <p>
      The main grid world should be very large, meaning that you'd have to travel long distances to get from one end of a continent or ocean to another. Distance and weight should matter, meaning that you'd need to use mounts, carts, wagons or ships to travel far, or to carry heavy loads.
    </p>
    <p>
      I’d make the world grid in 20x20 room pieces, which is a nice size to map on A4 graph paper. It would be easy to expand this grid successively, by just adding more 400-room pieces, with varying topography. But I'd keep it small, until the ecology system was balanced. Just two basic 400 room zones, one land based, the other water based.
    </p>
    <p>
      To these two basic grids I'd then add all the life forms: trees, plants and animals. Then I'd work at getting the ecology reasonably balanced within these two 400-room zones before starting to expand the world.
    </p>
    <p>
      I am inclined to think that the room descriptions in the basic grid won’t be very important, because all the interesting and dynamic things in the game would happen with the mobs and objects. So the grid descriptions would be short and concise, set by the code, and based on the terrain of each room.
    </p>
    <p>
      This is where the fun starts — because what would also be set by the code, based on the terrain on each grid square, would be a “fertility” value which would determine how much vegetation the square could sustain, and how fast it would grow. This value would be the basis for the entire ecology system. There should be a lot more terrain sectors than the usual stock list, and naturally terrains like “field” or “prairie” would have a much higher fertility value than “mountain” or “desert”.
    </p>
    <h3>Time, seasons and weather</h3>
    <p>
      Time would naturally play a big role in the ecology system, and not just day and night changes. There is stock code for seasons, with weeks, months and years, which could be developed further. For instance, when farming you'd have to prepare the soil in early spring, then sow, then wait for the harvest in autumn. The outcome of the crop would depend on good tending and timing, (i.e., if you didn't plough or sow at the right time, there would be nothing to harvest).
    </p>
    <p>
      The existing weather code does not add much to the game, just some messages about how it’s starting, or ceasing, to rain, etc. What I have in mind would have a much larger impact on the environment. It would be cold in winter, so you’d need warm clothes to survive. There would be the normal weather cycles, but also droughts and torrents, and occasional natural disasters, like tornados, wildfires, earthquakes and landslides.
    </p>
    <p>
      The weather changes would affect the cultivated crops more than the natural vegetation, which would be adapted to the climate. For instance, if it doesn't rain for an unusually long time, some crops will wither and die unless you water them. The opposite is also a danger: if it rained profusely during harvest time, the corn would rot and there'd be a crop failure.
    </p>
    <p>
      Seasons would be simulated through the vegetation. Instead of changing the room descriptions, you can work with the objects, showing the vegetation in different stages of development.
    </p>
    <p>
      I've made a pretty simple system of scripts for an orchard, where each tree loads a new version of itself for each new season before purging itself. So there are flowers and budding leaves in spring, green foliage in summer, ripe fruits in autumn, and bare twigs in winter. The fruits can only be picked in autumn.
    </p>
    <p>
      The same system could be used for all vegetation objects, like tufts of grass, herbs, flowers, bushes, trees, etc. These could all be shown in different stages of development based on the season and their life cycle.
    </p>
    <p>
      The drawback, of course, is that you'd need four different objects for each single species, but once those are created, the rest will be automatic.
    </p>
    <p>
      This brings us to the main feature:
    </p>
    <h3>The ecology system</h3>
    <p>
      The ecology system is controlled by five parameters:
    </p>
    <ul>
      <li>Fertility: The basic factor, set on each grid square, but also on each species, both plants and animals.</li>
      <li>Food chains: Plants → herbivores → smaller and larger carnivores.</li>
      <li>Life cycles: Birth → youth → mating → propagating → old age → death.</li>
      <li>Seasons: Most life cycles would equal a year’s time, but some are much longer.</li>
      <li>Supply and demand: The amount of food available determines the size of a population.</li>
    </ul>
    <p>
      To start out with the plants, I would make grass, herbs, bushes and trees (with leaves) as food objects, which the herbivores would feed from, and then make predators to feed from the herbivores. Most plants would have a life cycle on a yearly basis, and the fertility factor would determine how many plants of each kind a square can sustain. Some species, like trees, would have a much longer life cycle.
    </p>
    <p>
      I'd make similar life cycles for the animals, so that they would get born, grow up, mate, give birth, grow old and die, all within a set time. Again, this means at least four mobs for each species, but it should be well worth the extra effort.
    </p>
    <p>
      Some species would be bound to certain terrains, or have dens that they’d return to for sleep during day or night, but most of them would roam freely on the grid.
    </p>
    <p>
      I'd make several types of animals: insects, spiders, birds, fish, snakes, frogs and mammals. Each species would have a different set of properties, like size, fertility, speed, agility, tenacity, stamina, ferocity and strength. High stats in one of these factors would be balanced by lower stats in others. For instance, a rabbit would be high in fertility and speed and low in all other properties, while a bear would be high in strength and ferocity, and low in the others.
    </p>
    <p>
      Depending on these properties, an animal would hide, burrow, flee, evade or fight when threatened or attacked. All of them would have to eat and drink to survive.
    </p>
    <p>
      The herbivores would have to roam to find food and water once they’d consumed all the available vegetation in one square. The carnivores would feed from the herbivores, mainly from kids and old animals, and all mature animals would defend their offspring. Kids would follow their mothers, and herd animals would stick together.
    </p>
    <p>
      This could all be done with scripts, but should perhaps better be done by code. The important thing would be to maintain a balance, so that the populations are reasonably stable, unless tampered with by players. A population would be controlled and kept in place by several factors, like fertility, food supply, lifespan, size and strength, evasion ability, and natural enemies.
    </p>
    <p>
      There would also be edible mushrooms, vegetables, roots, fruits, nuts and seeds, which animals and humans would compete for.
    </p>
    <h3>Human Development</h3>
    <p>
      To this basic ecology, once it is balanced, I would then add the possibility for the players to farm the land and to fence it in, either to protect one’s farming field from herbivores or to protect livestock from carnivores. You should be able to domesticate some animals, but if you fence them in, you’d also have to feed and water them regularly.
    </p>
    <p>
      Farming a field would increase the original fertility factor with each step (plowing, harrowing, fertilizing, watering and sowing), and if started at the right time would yield a substantial crop to harvest and trade in the autumn.
    </p>
    <p>
      A vital part of the world would be a number of small villages, inhabited by human natives. These would be friendly, if approached peacefully, but they would also defend themselves and their possessions if provoked. Mainly they would serve as trading partners and mentors. They would buy your products, and in some cases help with developing them, or give advice on how you can do it yourself. Rare and hard-to-get items would fetch higher prices, and the more developed a product, the higher the price. Players would also barter and trade among themselves, encouraging player interaction and roleplaying.
    </p>
    <p>
      It would be a survival game, both short and long term, and the players would enter the world as the sole survivors of a spaceship crash on an almost virgin planet. They would be armed with a knife and a gun, which would give them a small advantage to start out with — but only as long as the bullets in the magazine last.
    </p>
    <p>
      To survive and raise their own status, the players can:
    </p>
    <ul>
      <li>Fish, hunt and collect berries, fruits, nuts and seeds for food.</li>
      <li>Kill and fillet most animals for food, and skin them for their hides.</li>
      <li>Fletch baskets from grass or fibers to be able to carry more items.</li>
      <li>Make primitive tools and weapons from flint, wood and bones.</li>
      <li>Fell trees and use the timber.</li>
      <li>Trade these basic resources with the natives and each other.</li>
      <li>Learn magic from the village shaman and become sorcerers themselves.</li>
      <li>Collect special plants for medicine and magic, and brew potions from them.</li>
      <li>Build willow huts for protection and storage.</li>
      <li>Plough and harvest the soil to produce larger crops.</li>
      <li>Fence in fields to protect them from roaming herds of grazing animals.</li>
      <li>Fence in livestock to protect it from carnivores.</li>
      <li>Shear sheep, milk cows or goats, and process the milk to butter and cream.</li>
      <li>Process animal skins to leather, to use for shoes, coats and armor.</li>
      <li>Card wool, and the stalks or seed pods of some plants, to produce textile fibers.</li>
      <li>Spin thread from fibers, weave cloth from the thread, and sew it into clothes.</li>
      <li>Pan and sluice for gold in mountain creeks.</li>
      <li>Mine for metal ores and melt them into alloys.</li>
      <li>Quarry for coal and stone, and cut the stone into blocks, slates and ashlars.</li>
      <li>Smelt ore, make alloys and forge them into tools, weapon blades and armor plates.</li>
      <li>Make more advanced weapons and armor from leather, metal, horn, bone and wood.</li>
      <li>Build carts, wagons and ships, to move heavy cargo.</li>
      <li>Build roads to travel faster and more safely.</li>
      <li>Build houses and castles from wood and stone, and, in the long term, build cities.</li>
    </ul>
    <p>
      The raw materials form the basis of the craft system, where you develop them into more advanced products which fetch a much better price. Everything you make will also decay over time, so the cycle must be kept going.
    </p>
    <p>
      Most of the development projects would also lay waste to a piece of land. Even building roads and turning grassland into farmland would affect the global ecology, mainly by reducing the available grazing areas for the wildlife and removing it from the ecology cycle.
    </p>
    <p>
      As long as the untouched areas and basic supplies are large enough, though, it shouldn’t crash the ecology system totally — this being one reason you need a really large grid.
    </p>
    <p>
      Still, too many bad choices on a too grand scale might result in a global disaster, comparable to a meteor strike. If the players actually manage to crash the system, it would mean permadeath for everyone and a full reboot of the world and player base.
    </p>
    <h3>Conclusion</h3>
    <p>
      Over the years bits and pieces of these ideas have been added to my own home MUD. We have farm fields, garden plots and orchards, a ranch to raise cattle in, and a large, working mine. You can fell trees, milk cows, shear sheep, skin most animals, and scrape and tan their hides into leather. There is also an extensive mob-run trading system in place. But it’s still just “bits and pieces”, and I’m really tempted to try the full ecology system out in a full scale.
    </p>
    <p>
      However, my home MUD — both the code and the world — has grown so large, complex and convoluted that in some ways it resembles the proverbial white elephant that’s so big, stubborn and heavy that almost every change of its course involves an inordinate amount of work. A radical upheaval, such as the basic ecology system I just described, would be next to impossible.
    </p>
    <p>
      It would be a lot easier to start out from scratch.
    </p>
    <p>
      But who’d do that at a time when text MUDs seem to be slowly dying?
    </p>
{% endblock %}
{% block article_bio_content %}
	Molly O’Hara is Head Builder on <a class="gametitle" href="http://4dimensions.org">4Dimensions</a>.
{% endblock %}
