{% extends "issue/article/index.html" %}
{% block page_title %}
	A modern interface for a modern MUD
{% endblock %}
{% block article_authors %}Richard <span class="author-nick">KaVir</span> Woolcock{% endblock %}
{% block article_date %}14th October 2013{% endblock %}
{% block article_content %}
  <p class="quotetext">
    Do you see novels in color? I don't, and prefer my mudding life to be that way too.
  </p>
  <p class="quoteattr">Robert Peckham, 8th June 1995</p>
  <p>
    Early MUDs were usually played through basic Telnet clients, which offered relatively few options to improve the presentation of output. The introduction of specialised MUD clients provided mudders with a range of new features to enhance gameplay, but the simple terminal interface remained the norm for many years, and is still used today by some players.
  </p>
  <p>
    A few MUDs used VT100 control codes to display status bars or clear the screen, but even they were rare; the options available for enhancing the interface were usually ignored or dismissed. Even the use of ANSI colour was initially disdained by many, and it was years before colour became a standard MUD feature. It should come as no surprise that the adoption of new standards is still slow in the modern age of mudding.
  </p>
  <div class="imgcenter">
    <img class="center" src="images/image02.png" width="602px" />
  </div>
  <h3>The rise of graphics</h3>
  <p class="quotetext">
    I think both are really the same thing; in many ways, there are far larger differences between certain kinds of text muds than there are between graphical and text-based games.
  </p>
  <p class="quoteattr">Raph Koster, 31st March 2006</p>
  <p>
    Although there were earlier graphical MUDs, such as <span class="gametitle">Habitat</span> in 1985, it wasn't until the mid to late 1990s that graphical MUDs really started becoming popular. Rebranded as MMORPGs in 1997 by Richard Garriott, they have continued to evolve and grow in popularity over the years, and there’s no disputing that they’ve become very successful.
  </p>
  <p>
    Yet the primary difference between a MUD and an MMORPG lies not in the <em>server</em> but in the <em>client</em>.
    (Koster 2006) Of course there are other differences, but they tend to be the same sort of things that differentiate one MUD server from another — choice of protocols, scaling for playerbase, combat and movement systems, content, and so on. These are all decisions that every server developer needs to consider, regardless of whether they’re designing a MUD or an MMORPG.
  </p>
  <h3>Graphics are not the enemy</h3>
  <p class="quotetext">
    I don't think there is much interest in adventure games anymore. Everyone wants graphical interfaces without text input... basically TEXT ADVENTURES ARE DEAD.
  </p>
  <p class="quoteattr">Colin Adams, 1st October 1990</p>
  <p>
    For decades, people have been claiming that text-based games are dead or dying. They’ll often accuse the MMORPGs of stealing their audience, while at the same time refusing to put any effort into client development. However, if you look at the most popular MUDs, you'll see that many of them <em>have</em> customised their own clients, designed their own interfaces, and retained a sizeable playerbase. While there are certainly other factors to be taken into account, the value of aesthetics should not be underestimated — it’s a lesson we can and should learn from MMORPGs.
  </p>
  <p>
    There are few people who would argue that aesthetics <em>aren't</em> important. Even for interfaces that are pure text, it makes a far better impression on prospective players if that text is nicely formatted, checked for spelling and grammar, uses colour to highlight important information, and so on.
  </p>
  <p>
    If you take a look at the websites for various MUDs, you'll see the use of fonts, colour, and graphics, all designed to improve aesthetics — after all, websites are important even for text-based games, and a well-presented website can give visitors a far better impression of your MUD.
  </p>
  <p>
    But then you need to ask yourself the question: if you're putting all that effort into making the website look nice, shouldn't you put at <em>least</em> as much effort into the appearance of the MUD itself? Would you use ASCII graphics on your website?
  </p>
  <div class="imgcenter">
    <img class="center" xheight="75" src="images/image01.png" width="602"/>
    <div class="imgcaption">Traditional text prompt, VT100 energy bars, and graphical energy bars. Three ways to display the same information — which would you rather use?</div>
  </div>
  <h3>Text versus graphics</h3>
  <p class="quotetext">
    Adding flashy graphics and whatnot won't keep people playing your game because there will be much better graphical games out there.
  </p>
  <p class="quoteattr">Quixadhal, 19th August 2013</p>
  <p>
    One of the more common objections to the use of graphics is based on a false dilemma — the mistaken belief that a good game has to be either graphical <em>or</em> text-based, and that it's not worth adding any graphics unless you can offer something on-par with the latest commercial MMORPGs.
  </p>
  <p>
    Yet there are many examples that prove otherwise. Take a look at a game like
   <a class="gametitle" href="http://www.travian.us/">Travian</a>,
   which has an very basic graphical interface layered over what could just as easily be played as a text-based game. Take a look at
   <a class="gametitle" href="http://www.ftlgame.com/">Faster Than Light</a>,
    which is effectively a roguelike with a fairly simple GUI, and yet it really <em>works</em>.
   The interface might be simple, but it complements the gameplay beautifully and makes the overall product feel polished as a result.
  </p>
  <p>
    However, at the end of the day, it shouldn’t be about what <em>other</em> games are doing, but about making the most of the tools available to improve the appeal of your <em>own</em> game. A well-designed MUD should have an intuitive and user-friendly interface, and certain aspects of an interface can be better represented through graphics than through text: a gauge is easier to read (and less spammy) than a prompt, a proper graphical map is easier to understand than an ASCII map, and so on. For blind players in particular, sound can go a long way towards making the game more accessible.
  </p>
  <p>
    Of course, a fancy interface won't turn a bad game into a good one — it goes without saying that you need to put effort into the gameplay as well. But conversely, if your MUD is poorly presented and the interface is ugly and clunky, it won't matter how many cool features you've got, because nobody will hang around long enough to give your game a chance. (Gray and Gabler 2005)
  </p>
  <div class="imgcenter">
   <img class="center" xheight="272" src="images/image04.png" width="291"/> <img class="center" xheight="271" src="images/image05.png" width="271"/>
   <div class="imgcaption">ASCII graphics are still graphics. If you’ve already decided to use ASCII graphics in your MUD, why not take it to the next level?</div>
  </div>
  <h3>Modern MUD clients</h3>
  <p>
    Text-based MUDs with graphical interfaces have been around for a while, but in the past they tended to use private clients and closed protocols. In recent years this has changed, with modern clients adding support for a range of new features, MUSHclient and Mudlet being particularly notable examples for their flexible graphical support. In the case of Mudlet, individual MUDs can even offer a custom script which the client downloads automatically when a player first connects, removing the entry barrier of having to download and install a separate interface for each MUD.
  </p>
  <p>
    The number of browser-based MUD clients has also been growing, with FMud being customised and integrated into a number of websites, and a relatively new contender called The MUD Portal offering a powerful and extensible interface. While not everyone likes playing a MUD through their web browser, such an option can significantly lower the entry barrier for new players, who might otherwise be required to download and install a client before they can play.
  </p>
  <p>
    There are even clients designed for smartphones and tablets, such as the feature-rich Blowtorch client for Android. While many clients offer data compression via MCCP, it becomes particularly important for smartphone clients, as they often have a limited data plan.
  </p>
  <div class="imgcenter">
    <img class="center" xheight="325" src="images/image00.png" width="601"/>
    <div class="imgcaption">A custom interface for MUSHclient, showing dungeon exploration.</div>
  </div>
  <p>
    In addition to looking better than ASCII, proper graphics can be a lot easier for players to understand. Take a look at the comparison below: the first map is ASCII and provides only a rough indication of where you are and what parts of the dungeon you’ve explored, while the second map is detailed enough to be useful for navigation.
  </p>
  <div class="imgcenter">
   <img class="center" src="images/areamap1.png" width="269"/> <img class="center" src="images/areamap2.png" width="248"/>
  </div>
  <h3>Modern client features</h3>
  <p>
    Out-of-band protocols such as MSDP, ATCP, and GMCP can be used to pass data transparently between the MUD and the client. This allows you to refresh maps, energy bars and other graphics in real-time without waiting for the text window to update.
  </p>
  <p>
    Most modern clients now support at least 256 colours (8-bit), while some support over 16 million (24-bit). Although this might seem excessive for text, it can be useful to have access to certain colours and shades that aren’t included with the normal 16 ANSI colours.
  </p>
  <p>
    Above and beyond 24-bit colour, MXP adds support for clickable links and menus, fonts, inline graphics, and a range of other features.
  </p>
  <p>
    Some clients also support Unicode characters such as the Norse runic alphabet, alchemical symbols, gender symbols, weather symbols, line-drawing characters, chess pieces, and so on.
  </p>
  <p>
    Sound is another useful option that’s particularly popular among blind and visually-impaired players, serving a role that’s both aesthetic and functional — the audible “clank” of a sword-blow is much faster to interpret than waiting for your screen reader to spit out an entire combat message. While blind players may be a minority, they’re also less likely to be lured away by fully graphical games, which tend to be less accessible than text-based MUDs. (Peters 2009)
  </p>
  <div class="imgcenter">
    <img class="center" xheight="363" src="images/image06.png" width="602" />
    <div class="imgcaption">A custom interface for Mudlet, showing a fight in the dojo.</div>
  </div>
  <h3>Modern MUD servers</h3>
  <p>
    Of course it doesn’t matter how fancy a client is if the MUD server doesn’t use any of its features! Graphics and sound are handled at the client end, but the MUD still needs to tell the client <em>what</em> and <em>when</em> to draw. You can theoretically do much of the work with triggers and regular expressions, and many players do indeed create their own interfaces, but that’s a poor substitute for official server-side support, not to mention a lot more effort for the players you’re trying to attract and retain.
  </p>
  <p>
    Many MUD owners seem to view graphical interfaces as some sort of mysterious black magic that requires a vast investment of time and skill. While it's certainly possible to spend a lot of time on GUI work, even a simple interface can be a huge improvement, and it's something that can be achieved extremely quickly; there are public snippets, like my own
    <a href="http://www.mudbytes.net/file-2811">MUD Protocol Handler</a>
    and Scandum’s
    <a href="http://www.mudbytes.net/file-2608">MUD Telopt Handler</a>,
    that provide the server-side support, and various scripts and plugins that can be used as a starting point for designing your own GUI. It should be possible for most MUD owners to have something functional in a matter of hours.
  </p>
  <p>
    The tools are all there. You just have to use them.
  </p>
{% endblock %}
{% block article_bio_content %}
    KaVir (Richard Woolcock) is the owner of <a href="http://www.godwars2.org/">God Wars II</a>.
{% endblock %}
{% block article_references_content %}
    {{ super() }}
  <p class="citation">
    Koster, Raph. 2006. “Are MUDs and MMORPGs the same thing?”
    <a href="http://www.raphkoster.com/2006/03/31/are-muds-and-mmorpgs-the-same-thing/">http://www.raphkoster.com/2006/03/31/are-muds-and-mmorpgs-the-same-thing/</a>
  </p>
  <p class="citation">
    Gray, Kyle and Gabler, Kyle. 2005. “How to Prototype a Game in Under 7 Days.”
    <a href="http://www.gamasutra.com/view/feature/2438/how_to_prototype_a_game_in_under_7_.php">http://www.gamasutra.com/view/feature/2438/how_to_prototype_a_game_in_under_7_.php</a>
  </p>
  <p class="citation">
    Peters, Matthew. 2009. “Spot On: The blind gaming the blind.”
    <a href="http://www.gamespot.com/articles/spot-on-the-blind-gaming-the-blind/1100-6215457/">http://www.gamespot.com/articles/spot-on-the-blind-gaming-the-blind/1100-6215457/</a>
  </p>
{% endblock %}
