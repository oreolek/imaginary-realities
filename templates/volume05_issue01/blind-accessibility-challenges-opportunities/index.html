{% extends "issue/article/index.html" %}
{% block page_title %}
	Blind accessibility: challenges and opportunities
{% endblock %}
{% block article_authors %}Matthew <span class="author-nick">Chaos</span> Sheahan{% endblock %}
{% block article_date %}30th October 2013{% endblock %}
{% block article_content %}
    <p>
      In the years after the decline of the overall MUD player base from its heights in the 1990s, I gradually noticed that one group of players was particularly stubborn in its attachment to MUDs: blind and visually-impaired people. It made sense: in a gaming world focused on visual experiences, text-based MUDs relied on an interface practically designed for <a href="http://en.wikipedia.org/wiki/Screen_reader">screen readers</a> (the class of software generally used to make the Internet accessible to those who can’t rely on vision). My interest in blind accessibility in MUDs has slowly increased since then, and I’ve come to regard this area as one where MUDs in general face significant challenges, but also meaningful opportunities.
    </p>
    <p>
      I spoke to Dennis “Dentin” Towne, lead coder for <a class="gametitle" href="http://www.alteraeon.com/">Alter Aeon</a>, the MUD widely regarded as the leader in blind accessibility, and to Hussain “Fate” Jasim, blind mudder and developer on my home MUD, <a class="gametitle" href="http://lostsouls.org/">Lost Souls</a>.
    </p>
    <h3>The bad news</h3>
    <p class="quotetext">I'd say it's as difficult to get MUD players in the blind community as it is in the sighted.</p>
    <p class="quoteattr">Dennis Towne</p>
    <p>
      Towne is quick to caution us that blind accessibility cannot simply be a matter of adding a slapdash handful of features and declaring victory. Meaningful accessibility improvements are best achieved by an ongoing dialogue between developers and the community to identify “what is most needed for a particular game, and what is most helpful”. (Towne 2013)
    </p>
    <p>
      Towne has seen a distressing prevalence of MUDs that “treat the blind rather poorly, like second class citizens or idiots”. Obviously, this isn’t the sort of reception we should be giving any players, particularly not because of a disability.
    </p>
    <p>
      Jasim and Towne both identify spam, in particular combat spam, as one of the biggest accessibility problems in MUDs. On many MUDs, if a screen reader were to output speech at the rate text scrolls by, it would be gibberish, which makes the availability of output filtering a critical issue. Both the server side and the client side can help with this. Server-side filters such as suppression of output from other people fighting in the same room, brief room descriptions, and skipping missed attacks provide both direct benefits, by reducing output at the source, and indirect, by allowing client-based filters to address a more targeted set of cases. Constructing messages so that different types of events have distinctive messages, and preventing those from being spoofed, also helps considerably with designing client-side triggers and filters. At an advanced level, out-of-band data can be attached to messages that gives the client even more to work with. (Jasim 2013, Towne)
    </p>
    <p>
      The second most problematic element common to MUDs is ASCII art, such as pseudographical maps and drawing-character-based diagrams. These are simply nonsense to screen readers, and if vital information is only available through them, that will place blind players at a great disadvantage. Some MUDs address this by providing entirely different output from relevant systems if the player is known to be using a screen reader. (Towne, Jasim)
    </p>
    <p>
      Another design choice that can have an outsized negative impact on blind accessibility is over-reliance on ANSI or MXP color. (Towne) To keep from damaging accessibility, color should be used such that whatever information it conveys is also available through the text. Color should never be the only way to find out significant information. Note that this is as relevant for how your game is experienced by colorblind players as those with more severe visual impairments.
    </p>
    <p>
      Unfortunately, blind accessibility resources available on the Web are of little use in improving the accessibility of MUDs, as they tend to be focused on traditional GUI applications and websites. The text-rich, real-time environment of a MUD has needs that aren’t well-understood in the broader community. (Towne)
    </p>
    <p>
      Towne cautions that, while MUDs have particular appeal for blind players — “the bulk” of <span class="gametitle">Alter Aeon’s</span> player base is blind — this doesn’t necessarily mean that blind players are the strongest of markets for MUDs to draw players from. Like sighted gamers, they’re largely drawn to other genres, some of which are specifically designed for audio-based play. Jasim is the only person who plays MUDs among the visually-impaired people he knows. Towne credits <span class="gametitle">Alter Aeon’s</span> success with blind players mainly to “being an active part of the community for a long time”. He and Jasim both see active engagement and awareness-raising with blind gamers as an important key to successful accessibility efforts.
    </p>
    <p>
      Developing for blind accessibility can be difficult for the sighted simply because it’s an unfamiliar mindset to most. Even in an accessibility-focused MUD, it isn’t unusual to implement a new feature only to realize it needs to behave differently to facilitate accessibility. (Towne)
    </p>
    <h3>The good news</h3>
    <p class="quotetext">MUDs are great… They allow me to explore my imagination and experience the same love of gaming that my peers do while playing MMORPGs.</p>
    <p class="quoteattr">Hussain Jasim</p>
    <p>
      The starting point of most text-based MUDs is already relatively accessible. Mere reliance on verbal, non-ASCII-art content for the basic interface puts a MUD head and shoulders above most of the software and websites in the world. (Jasim)
    </p>
    <p>
      The most effective way to begin improving accessibility is simply to engage with blind players — either those you already have, and more MUDs have blind players than necessarily realize it, or those on sites for blind gamers like <a class="website" href="http://audiogames.net/">audiogames.net</a> — and listen to their feedback. (Towne, Jasim) Many MUDs have places where a little bit of work can mean a lot of improvement to blind players’ experiences.
    </p>
    <p>
      The MUD community has proven technologies available, like <a href="http://www.zuggsoft.com/zmud/msp.htm">MSP, the MUD Sound Protocol</a>, that can greatly improve accessibility. MSP sound packs are an important part of <span class="gametitle">Alter Aeon’s</span> accessibility approach, Towne having been inspired to add this functionality by the MOO <a class="gametitle" href="http://www.toastsoft.net/">Miriani</a>. (Jasim, Towne)
    </p>
    <p>
      Blind accessibility doesn’t have to mean abandoning powerful ASCII-art-based features entirely. <span class="gametitle">Lost Souls</span> follows an approach of supporting ASCII art features for sighted players and providing alternate forms of those features’ output for those registered as using screen readers. (Jasim) It’s important in approaches like this, though, to “prevent any one group from feeling like they're at a disadvantage to another, which actually does matter for mixed crowds”. (Towne)
    </p>
    <h3>Why blind accessible?</h3>
    <p class="quotetext">I think the only way MUDs will be significant in making accessible gaming experiences available in the future is if the MUD community cooperates with the blind community, such as by including MUD clients on braille notetakers and becoming visible on blind mailing lists.</p>
    <p class="quoteattr">Hussain Jasim</p>
    <p>
      So, then, are the opportunities here worth taking on the challenges? Why prioritize blind accessibility?
    </p>
    <p>
      For any MUD developer traumatized by living under the shadow of mass-market AAA MMORPGs, there’s a simple and superficially compelling argument that a blind player is one who will never abandon a MUD for the latest shiny graphics. That’s a limited perspective that may lead to unreasonable expectations, though, since blind players cycle through games like anyone else, and as Towne points out, they have plenty of options available. So the siren song of loyal players is probably not the best way to motivate developers toward accessibility efforts.
    </p>
    <p>
      Possibly the most compelling reason is social responsibility. I don’t mean this strictly in the sense that it’s ethically desirable to provide accommodations for people with disabilities, though that’s true and relevant. I mean it equally in the sense that <em>these are our players</em>, and as developers we have a measure of responsibility in the experience that they have in playing our games. That this experience is different from ours, if we are sighted, doesn’t particularly alter this consideration. So if, by listening to the feedback of my players and spending some of my time, I can improve the experience my game provides to a number of people over many hours, I call that a win.
    </p>
    <p>
      Beyond the immediate gains, though, I think work done on accessibility has the potential to grant MUD developers much-needed perspective in the area of user experience design. It essentially forces us over what can be a difficult hurdle in that area: understanding the user’s experience as meaningfully different from our own, and validly so. Reaching that perspective, whether through working on blind accessibility or otherwise, could turn out to be the difference between a good MUD developer and a great one.
    </p>
{% endblock %}
{% block article_bio_content %}
	Chaos (Matthew Sheahan) is the lead developer of <a class="gametitle" href="http://lostsouls.org/">Lost Souls</a> and the maintainer of <a class="website" href="http://mudseek.com/">MUDseek</a>.
{% endblock %}
{% block article_references_content %}
    {{ super() }}
    <p class="citation">Towne, Dennis. Interview by author. Email. August 15, 2013.</p>
    <p class="citation">Jasim, Hussain. Interview by author. Email. October 30, 2013.</p>
{% endblock %}
