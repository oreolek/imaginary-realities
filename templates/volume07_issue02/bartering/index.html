{% extends "issue/article/index.html" %}
{% block page_title %}
	A Case For Bartering In Multiplayer Games
{% endblock %}
{% block article_authors %}<span class="author-nick">Griatch</span>{% endblock %}
{% block article_date %}19th February 2015{% endblock %}
{% block article_byline %}
	{{ super() }}
	<span class="italicd">images from <a href="http://griatch-art.deviantart.com/">griatch-art.deviantart.com</a></span>
{% endblock %}
{% block article_content %}
	<p class="rcaptionblock"><img alt="Troll_deal_by_Griatch_art.jpg" src="images/image01.jpg" style="width: 218.02px; height: 308.50px;"></p>
	<p>Most multiplayer online games eventually introduce some sort of economic system. Maybe there are NPC vendors to sell to, maybe there is an auction house or just the ability to
	trade with one another. An economic system of some description is expected in most games, if only for offloading your loot or upgrading your weapons.&nbsp;In any sort of
	consistent-world RPG it is also realistic &mdash;&nbsp;it makes sense that the in-game metropolis should have a burgeoning marketplace.</p>

	<p>From a game designer perspective, though, in-game economy has only one <span class="italicd">actual</span>&nbsp;purpose: to produce <span class="italicd">emergent gameplay</span>. Emergent gameplay roughly means that your players create their own game enjoyment out of the building blocks you supply - potentially extending the game beyond the sum of its parts.</span></p>

	<p>Economy is a great candidate for emergent gameplay. Players are inherently&nbsp;familiar with the concept of trade. By even introducing the <span class="italicd">possibility</span>&nbsp;to buy and sell, you&nbsp;give players the ability to create their own motivations and plots. It gives them stories of how they finally gathered enough money to buy that upgrade or managed to corner the market on rat tails. Fun all around!</p>

	<p>&hellip; In theory, anyway. In reality, game economy is <span class="italicd">tricky.</span>&nbsp;The problem with economy is that for it to be useful and fun, players must perceive it as completely fair yet rewarding to manipulate.  This article will try to argue against a single-currency <span class="italicd">money</span>-based economy and try to make a case for using a multi-currency <span class="italicd">bartering</span>&nbsp;economy instead.</p>

	<h3>The peaceful NPC world<p class="rcaptionblock"><img alt="Christmas_Card_2009_by_Griatch_art.jpg" src="images/image03.jpg" style="width: 303.51px; height: 214.50px;"></p></h3>

	<p>Consider an ideal money-based game economy.</p>

	<p>This economy is a simple closed market <span class="italicd">only</span>&nbsp;populated by computer-controlled NPCs. It has a fixed total number of money units &mdash;&nbsp;let&rsquo;s call it &ldquo;gold&rdquo; for the sake of this article. In our simulation we use some sort of AI to make sure NPCs have needs and can produce resources. Whenever an NPC buys something from another NPC, money changes hands but the total amount of gold in the world does not change. It&rsquo;s not too hard to emulate supply and demand: just track the global number of goods you have and (with some suitable algorithm) scale up the price for rare goods while scaling down the price for common ones. The system need not be completely deterministic, either; it can have some random ups and downs in goods availability. This is coupled to an artificial delay before a price adjusts to the &ldquo;real&rdquo; supply and demand, allowing NPCs to earn extra before prices drop or lose out if they happen to sell at the wrong time.</p>

	<p>This is a relatively simple system to code and emulates the overarching principles of a closed market; The NPCs trade their goods for gold, then spend that gold again for things they need. Some would benefit from the random chance and delays and get richer than others, others would become poor. This is a place of well-behaved NPCs. If you tweaked the numbers right you should realistically be able to balance and run this simulation indefinitely.</p>

	<h3>The humans come and ruin everything</h3>

	<p>Now introduce human players into this peaceful NPC economy and things tend to go haywire. Why is that? Here are only some of the reasons:</p>

	<ul>
	<li>Players join the game from nowhere, and they expect to be able to participate <span class="italicd">as if they were always there</span>. The real-world equivalent might be foreigners moving into a very isolated country, bringing new wealth with them. To avoid taking money from existing players, the economy must increase, meaning the <span class="italicd">total</span>&nbsp;amount of gold (and goods, but usually only gold is considered) must increase.</li>
	<li>A player leaving the game might <span class="italicd">or might not</span> return to the game again. This has no real-world equivalent. The character of a logged-out player usually has <span class="italicd">no</span> in-game needs, the money they own is locked away from the economy. You usually don&rsquo;t want to erase/reclaim their resources in case absent players decide to come back later. This causes a dilemma: Should the money they carry no longer count towards the total amount of gold? This effect may be minor in a large game like <span class="italicd">EVE Online</span>, but in a MUD with some 20-40 regular players, every player&rsquo;s purse matters.</li>
	<li>A player expects to have <span class="italicd">fun.</span> Or at least have some real influence over their own fate. This actually has no real-world equivalent either. In the real world you just have to live with a crashing economy or being stuck in a financial situation you can&rsquo;t get out of. In a game, you can log off and not come back.</li>
	<li>For the sake of ensuring &ldquo;fun&rdquo;, players tend to get unfair advantages compared to NPCs. They have access to goods/loot the NPCs cannot get and often travel much faster and over greater distances (this last part is very important &mdash;&nbsp;if movement is too fast, the real-world purpose of trade routes falls away).</li>
	<li>Many games introduce new gold into the system via monster kills and quest rewards. This is <span class="italicd">new</span>&nbsp;gold. No one ever traded that gold or used it to buy existing goods. Again, this is used as an incentive to keep the players entertained but it is death to our ideal little economy simulation.</li>
	<li>Finally, human players can&rsquo;t be expected to show restraint the way NPCs can be coded to do. Our nice supply-and-demand algorithm can easily be subverted if one player decides to spend a week cutting down all the trees in your game. It can be very hard to dampen the effects of such extreme actions.</li>
	</ul>

	<p>Note that from among these points, the irrational human nature is only mentioned in the last one&nbsp;(you must always expect a bored player to test the unexpected just for laughs). The rest stem from the fact that we not only want new players to join our game, we also want them to enjoy their experience.</p>

	<p>The unfortunate result of all this is that the economy becomes more and more meaningless with time. Rapid inflation kicks in: as more and more money is introduced into the system, the actual value of each unit drops. Few games even bother starting from that balanced and friendly NPC economy &mdash;&nbsp;they just open to human players and
	hope for the best. Most give&nbsp;new players a chunk of money and mint&nbsp;new coin for them whenever needed. NPCs are only there to dispense even more money, often at fixed prices. Before long players bathe in money, making the gold economy all but superfluous except to the newbies. That impressive sword you worked so hard to afford a few months back? Now it&rsquo;s worth no more than what you regularly carry in your pocket.</p>

	<p>But there is still a need to trade! Just not for money. At this point it makes sense that unofficial bartering emerges between players, eschewing NPC vendors and gold except for basic things they can&rsquo;t get otherwise.</p>

	<p>There are many ways proposed to solve the problems of in-game money economies (short of cheating and alienating your players by removing all their wealth by some hand-wavy fiat). Most deal with the concepts of <span class="italicd">sinks</span>. Sinks are basically black holes to throw gold into. This could be expensive one-use potions that people need to survive, taxes on transactions or other expenses that effectively removes gold from the system. In theory, if there are as many sinks as there are sources of money you would have an inflation-free economy where things retain their value with time.</p>

	<p>However, players loathe being forced to spend money (taxes are no more fun in-game than they are in the real world) and tricking them into <span class="italicd">wanting</span> to do so puts a lot of effort on the designer. It&rsquo;s probably possible to make a stable money economy but it&rsquo;s safe to say that most games with a traditional money economy <span class="italicd">will</span>&nbsp;experience rampant inflation eventually.</p>

	<p>That &ldquo;easy way to have players create their own content&rdquo; doesn&rsquo;t sound quite so easy anymore, does it?</p>

	<h3>Go for bartering right away
	<p class="rcaptionblock"><img alt="mother_s_day_flower_2014_by_griatch_art-d7jnc3j.jpg" src="images/image02.jpg" style="width: 250.50px; height: 250.50px;"></p></h3>

	<p>A prospect I think could be explored further in multiplayer games would be to forego the single-currency money system completely and go for the multiple-currency barter economy out of the box. This is not a new idea (Richard Bartle argues this point already in his seminal book <a href="http://en.wikipedia.org/wiki/Designing_Virtual_Worlds">Designing Virtual Worlds</a>), but it bears repeating: once your gold is rendered worthless by inflation, people will resort to bartering anyway, so why not make the barter system a robust one from the onset?</p>

	<p>The simplest way to barter is to simply agree to a trade and then use the game&rsquo;s <span class="command">give</span><span>&nbsp;command to do the exchange. There are some good reasons to add a more formalized barter mechanic to your game, though:</span></p>

	<ul>
	<li>Having a coded mechanic makes bartering an &ldquo;official&rdquo; part of the game in the mind of the players. It tells them that this is the intended way to do it.</li>
	<li>A barter system can make sure that no one holds both goods at the same time, avoiding real-life scammers &mdash;&nbsp;this could be a real problem if PvP is otherwise limited. If an in-game thief wants to pull a scam or take off with the goods, this should probably be tied to an actual game skill instead.</li>
	<li>A formalized barter system is easier to adopt for use with NPC traders using the same commands.</li>
	<li>A money economy is actually just a <span class="italicd">subset</span>&nbsp;of a barter economy. The only difference is that one side trades <span class="italicd">coins</span>&nbsp;rather than goods. So even if you do choose to have a money economy, a robust barter system allows you to trade everything seamlessly.</li>
	</ul>

	<p>Some years back I added such a safe barter system to Evennia&rsquo;s optional <span class="command">contrib/</span>&nbsp;folder. A MUD session in this system can look like this when player Ada wants to trade with player Borek:</p>

	<p><span class="boldd">Ada&gt;</span>&nbsp;<span class="command">trade Borek: Hi, I have a nice extra sword. Wanna trade?</span>
	<br/><span class="boldd">Borek&lt;</span>&nbsp;<span class="command">Ada says, &ldquo;Hi, I have a nice extra sword. Wanna trade?&rdquo;</span> <span class="italicd command">[Ada wants to open trade with you. Enter &lsquo;trade Ada&rsquo; to accept]</span></p>

	<p>As seen, these commands all support an optional additional say/emote for roleplaying (see the <a href="http://journal.imaginary-realities.com/volume-07/issue-01/choosing-an-emoting-system/index.html">last issue of Imaginary Realities</a>&nbsp;where I discussed this idea of extending normal commands with emotes).</p>

	<p><span class="boldd">Borek&gt;</span><span>&nbsp;</span><span class="command">trade Ada: Hm, I could use a sword &hellip; if the price is right.</span>
	<br/><span class="boldd">Borek&lt;</span> <span class="italicd command">[You are now trading with Ada. Use &lsquo;help trade&rsquo; for aid.]</span>
	<br/><span class="boldd">Ada&lt;</span><span>&nbsp;</span><span class="command">Borek says, &ldquo;Hm, I could use a sword &hellip; if the price is right.&rdquo;</span> <span class="italicd command">[You are now trading with Borek. Use &lsquo;help trade&rsquo; for aid.]</span></p>

	<p>At this point, Ada and Borek are in &ldquo;trading mode&rdquo;. This means that a subset of trading commands are available to let them offer up items for trade. Should either of them decide to move away (or, say, attack each other), the trade is aborted. No actual objects change hands yet. The system also makes sure that the players actually carry the items they are offering.</span></p>

	<p><span class="boldd">Ada&gt;</span><span>&nbsp;</span><span class="command">offer sword: This is a nice sword. I need some healing potions. Got any?</span>
	<br/><span class="boldd">Borek&lt;</span><span>&nbsp;</span><span class="command">Ada says, &ldquo;This is a nice sword. I need some healing potions.</span> <span class="italicd command">Got any?&rdquo; [Ada offers Sharp sword. Use &lsquo;inspect` to examine it]</span>
	<br/><span class="boldd">Borek&gt;</span><span>&nbsp;</span><span class="command">inspect sharp sword</span>
	<br/><span class="boldd">Borek&lt;</span> <span class="italicd command">(sees</span> <span class="italicd command">stats and description of the Sharp sword)</span>
	<br/><span class="boldd">Borek&gt;</span><span>&nbsp;</span><span class="command">offer 2 healing potion: Nice. Here are two potions for your sword.</span>
	<br/><span class="boldd">Ada&lt;</span><span class="italicd command">&nbsp;Borek says, &ldquo;Nice. Here are two potions for your sword.&rdquo;</span> <span class="italicd command">[Borek offers 2 potion of healing, use &lsquo;inspect&rsquo; to examine them]</span></p>

	<p>Ada does not think two healing potions are enough for the sharp sword. She does not have to make a counter-offer every time though; the normal emote/say commands work too:</p>

	<p><span class="boldd">Ada&gt;</span><span>&nbsp;</span><span class="command">say Hey, this is a nice sword. I need something more. Maybe some food?</span>
	<br/><span class="boldd">Borek&lt;</span> <span class="italicd command">Ada says, &ldquo;Hey, this is a nice sword. I need something more. Maybe some food?&rdquo;</span>
	<br/><span class="boldd">Borek&gt;</span><span>&nbsp;</span><span class="command">offer 2 healing potion, 3 rations: Alright, I&rsquo;ll throw in some quality rations, that&rsquo;s my final offer.</span>
	<br/><span class="boldd">Ada&lt;</span><span>&nbsp;</span><span class="command">Borek says, &ldquo;Alright. I&rsquo;ll throw in some quality&nbsp;rations</span><span class="command">, that&rsquo;s my final offer.&rdquo;</span> <span class="italicd command">[Borek offers 2 potion of healing, 3 big rations. Use &lsquo;inspect&rsquo; to examine them]</span></p>

	<p>At this point the offer is one sword for two healing potions and a few rations. The players have commands to list the current offering and continue bartering. If the deal is off, they can <span class="command">decline</span>&nbsp;to abandon the trade entirely. To seal the deal both sides must <span class="command">accept</span>&nbsp;the same standing deal:</p>

	<p><span class="boldd">Ada&gt;</span>&nbsp;<span class="command">accept: You are killing me here, but alright.</span>
	<br/><span class="boldd">Borek&lt;</span><span>&nbsp;</span><span class="command">Ada says, &ldquo;You are killing me here, but alright.&rdquo;</span> <span class="italicd command">[Ada accepts the offer. Use &lsquo;accept&rsquo; or change the offer to continue bartering]</span></p>

	<p>If Borek would change his offer now, Ada&rsquo;s registered acceptance resets so that she has to re-enter it again. Borek is happy with the deal, though:</p>

	<p><span class="boldd">Borek&gt;</span><span>&nbsp;</span><span class="command">accept: Excellent. Nice making business with you!</span>
	<br/><span class="boldd">Borek&lt;</span><span>&nbsp;</span><span class="italicd command">[you both accepted. Deal is made and goods change hands]</span>
	<br/><span class="boldd">Ada&lt;</span><span>&nbsp;</span><span class="command">Borek says, &ldquo;Excellent. Nice making business with you!&rdquo;</span> <span class="italicd command">[you both accepted. Deal is made and goods change hands]</span></p>

	<p>At this point the barter mode is exited and normal gameplay recommences. The promised items are automatically transferred between the players&rsquo; inventories.</p>

	<h3>Drawbacks of bartering</h3>

	<p>In a bartering system the single currency of money is replaced with multiple currencies &mdash;&nbsp;the goods themselves.</p>

	<p>The drawbacks of in-game bartering are the same as the drawbacks in the real world: What if you don&rsquo;t have what the other person wants? A barter economy is very inefficient in larger societies as people specialize in wares that not everyone wants all the time. Money exists for a reason &mdash;&nbsp;it greases the wheels of commerce. This means that you must design the game so that players require a wide range of goods to be successful at what they do &mdash;&nbsp;they must create supply and demand. All items in the game must be useful in some way so as to give them complementary trade value. A crafting system is a natural consequence. The goods need not be equally diverse for all games, though. One could picture a combat-oriented game where only weapons and armor are tradeable. For this to work one would have to design all weapons having their pros and cons and none being universally better than the rest &hellip; come to think of it, that sounds like good design advice overall!</p>

	<p>In small games with limited user bases the limits of bartering actually offer&nbsp;some interesting gaming and roleplaying possibilities. It gives players with desirable goods the incentive to set their price: goods that other Players need to procure for them &mdash;&nbsp;that is emergent gameplay right there. This can help tie the economy more closely to non-violent game mechanics like crafting, potentially making the latter more diverse and rewarding. Combat still has a role (where else to get wolf pelts and lizard hides?) but the spoils of battle are not as generally useful if you don&rsquo;t have the right buyers lined up, just as in the real world.</p>

	<h3>Conclusions</h3>

	<p class="rcaptionblock"><img alt="The_Human_pit_stop_by_Griatch_art.jpg" src="images/image00.jpg" style="width: 314.83px; height: 222.50px"></p>

	<p>An economic system is not something you should just put into your game as an afterthought because it &ldquo;needs to be there&rdquo;. There are great gameplay benefits to be had from an economy more complex than &ldquo;throw gold at the players until they stop complaining&rdquo;</p>

	<p>Although this article argues for the benefit of a barter (multi-currency) economy, a money (single currency) economy is probably&nbsp;easier to create. Tracking all these currencies is more work than counting gold. But it also means that the currency of your world is spread more broadly &mdash;&nbsp;it&rsquo;s harder to crash or inflate all those currencies at the same time. You can be the sole supplier of lizard livers but be low on snake tongues ... And any day now those crafty witches will figure out that snake tongues work just as well as lizard livers in their potions.</p>
{% endblock %}
{% block article_bio_content %}
	Griatch is the lead developer of the MUD-development system <a class="softwaretitle" href="http://www.evennia.com">Evennia</a>.
{% endblock %}
