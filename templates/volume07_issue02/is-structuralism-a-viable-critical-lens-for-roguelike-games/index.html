{% extends "issue/article/index.html" %}
{% block page_title %}
	Is Structuralism a Viable Critical Lens for Roguelike Games
{% endblock %}
{% block article_authors %}Matthew Ward{% endblock %}
{% block article_date %}31st January 2015{% endblock %}
{% block article_content %}
	<p>At first glance, one might want to throw out structuralism for roguelikes as nonsense. One of the genre&rsquo;s defining characteristics is that level design, character formation, enemy spawns, and so on are all random. It is futile to find structure in randomness. On the other hand, any roguelike developer will tell you the game is made of systems that interact. Almost by definition, a game is all structure.</p>

	<h3>A Brief History</h3>

	<p>Structuralism began in the early 1900s as a movement in linguistics. Structuralists&nbsp;such as&nbsp;Saussure&nbsp;attempted to describe how we get meaning from language by analyzing the structure of sentences. They claimed words get their meaning by how they fit together. The same word can take on a different meaning by rearranging the structure of the sentence.</p>

	<p>By the mid 1900s the philosophy of structuralism had spread to most other academic disciplines. In anthropology, cultural rituals obtain significance by understanding their structure and function in the society as a whole. In art, individual components of a film or painting can be broken up and examined in relation to the structure of the whole.</p>

	<p>Structuralism&rsquo;s widest success was in the analysis and criticism of literature. In modern days, we often speak of analyzing a work &ldquo;through a structuralist lens&rdquo; to emphasize that the interpretation is only one of many ways to approach a work of art. A structuralist critique of a roguelike game will be considered successful if it teaches us how the game creates a meaningful experience for the player.</p>

	<h3>What is Game Criticism?</h3>

	<p>Before we begin, what is meant by &ldquo;criticism&rdquo;? This term has become a catch-all in literature. It refers to describing or evaluating how well a work creates meaning. Ian Bogost wrote an article, &ldquo;Comparative Video Game Criticism&rdquo;,<sup>1</sup>&nbsp;for <span class="publicationtitle">Games &amp; Culture</span>&nbsp;which addressed what the transference of these literary techniques to games would and should look like. To my knowledge, no one has actually attempted to carry out this activity until now.</p>

	<p>The most prominent thinker to use structuralism to find meaning in literature was Roland Barthes, and I will use his essay, &ldquo;Introduction to the Structural Analysis of Narratives&rdquo;,<sup>2</sup>&nbsp;as a template. In the essay, he extrapolates three key levels of structure that occur in a literary work and goes on to describe how they interact with each other to create meaning for the reader.&nbsp;Likewise, I will identify three levels of structure in roguelike games. To become competent, the player must identify these levels and understand how they work together.</p>

	<p>A game critic that uses structuralism in an analysis can examine whether these structures work together to create a coherent and interesting gaming experience. We will use <span class="gametitle">NetHack</span> as a running example due to its popularity with roguelike aficionados.</p>

	<h3>The Lowest Level of Structure</h3>

	<p>The smallest level of structure occurs at the level of the symbol on the computer screen. It is sometimes difficult to notice in ordinary language, but Saussure argued that every word is a symbol signifying something else. In an ASCII/ANSI symbol-based roguelike game, the fact that the symbols on the screen signify something else is far more obvious:</p>

	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image00.png" style="width: 515.79px; height: 260.57px;" title="" />
	  </div>

	<p>In <span class="gametitle">NetHack</span>, the symbol <span class="code">.</span>&nbsp;refers to a free space; <span class="code">#</span>&nbsp;a corridor; <span class="code">|</span>&nbsp;a wall; <span class="code">@</span>&nbsp;your avatar; <span class="code">x</span>&nbsp;a grid bug; <span class="code">!</span>&nbsp;a potion; <span class="code">+</span>&nbsp;a door, and so on. Each of these symbols carries with it a structure. In the case of <span class="code">@</span>, it carries a great deal of structure: your HP, intelligence, weapons, items, and so on. In the case of <span class="code">!</span>, it only carries the structure of the potion.</p>

	<p>This first level of structure is interesting enough in its own right that Mark Johnson wrote a forthcoming paper on it entitled &ldquo;The Semiotics of the Roguelike&rdquo;.<sup>3</sup>&nbsp;It explores how the naming of these symbols has enough of a structure that an experienced player can come upon a symbol they haven&rsquo;t seen before, such as an <span class="code">H</span>, and intuit what it means. The letter <span class="code">h</span>&nbsp;usually means a humanoid creature, and the capitalization shows the player it is large. Thus <span class="code">H</span>&nbsp;must be some sort of giant, and the player guesses correctly.</p>

	<p>It is weird to ask whether a game &ldquo;succeeds&rdquo; at this lowest level of structure, but for the sake of criticism, one could argue a game has failed structurally if an experienced player can never form an intuition for how a developer chose the displayed characters or what a logical choice for a new monster would be. One can imagine a similar situation in literature where an author chooses a wrong word that obscures the meaning.</p>

	<p>Barthes classifies these smallest units into types, but I don&rsquo;t think it is worth inventing types for roguelikes, because they already exist. Some symbols are items (they have a functional role). Some are NPCs (these create conflict). Some are ambience (these build the world). The player will become familiar with these type classifications depending on the roguelike.</p>

	<h3>Higher Level Structure</h3>

	<p>The next level of structure is how each member of the first level interacts. A dungeon level in NetHack comprises several rooms linked by corridors. The fact that this structure is persistent and noticeable across games, despite the procedural generation, can be seen when an experienced player notices a change in a <span class="gametitle">NetHack</span> variant like <span class="gametitle">SporkHack</span>. For simplicity, we will consider the whole level to be this structure:</p>

	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image01.png" style="width: 569.91px; height: 277.84px;" title="" />
	  </div>

	<p>In a more detailed analysis, it might be useful to consider individual rooms. The structure of the level dictates much of the move-to-move strategy and gameplay. For example, corridors tend to be one character wide. A strategy most players pick up early on is to kite several enemies into a corridor, so you can fight them one at a time rather than all at once.</p>

	<p>To see how a minor structural change can lead to a vastly different play experience, consider a roguelike in which hallways are two spaces wide (e.g. <span class="gametitle">Sword of the Stars: The Pit</span>). The hallway kiting strategy isn&rsquo;t merely less effective, but is almost no different than fighting in a large room.</p>

	<p>This second level of structure includes interaction between&nbsp;characters from&nbsp;the first level. Barthes calls his second level the &ldquo;action&rdquo; level, and we will keep this terminology here. The way <span class="code">@</span>&nbsp;and <span class="code">!</span>&nbsp;interact is through an action, whether it be to pick it up or quaff. The way <span class="code">@</span>&nbsp;and <span class="code">x</span>&nbsp;interact is through an action. The dungeon layout is how the player interacts with the environment.&nbsp;This action structure dictates the turn-to-turn strategy which many players never get beyond.</p>

	<p>The largest scale structure is the most interesting and most often overlooked. I&rsquo;ve read many forum discussions on roguelike strategy. Some people do not even consider traditional roguelikes to be strategy games at all. At the outset, it seems as if the strategy amounts to mere survival from room to room: random enemies spawn and you must decide whether to run or attempt to kill them.</p>

	<p>This mentality of the small-scale strategy may work on early levels, but experienced players that understand the large-scale structure take a much different approach. The large-scale structure of <span class="gametitle">NetHack</span> is complicated to describe in words, but roughly has four segments (labelled in red):</p>

	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image02.png" style="width: 765.85px; height: 531.02px;" title="" />
	  </div>

	<p>The game starts in the Dungeons of Doom. The second main part is the Quest. The third main part is Gehennom, and the last part is the Astral plane. Each of these can be divided further into subsections and special levels to form this large-scale structure. For example, the Dungeons of Doom have two early (dead end) branches: Sokoban and the Gnomish mines.</p>

	<p>Understanding this structure is vital to forming a solid long-game strategy; you must understand what items and equipment you need and approximately how strong you should be to enter various large-scale sections. An example of this&nbsp;is at the end of the Sokoban branch, where you have a 50% chance of finding a bag of holding. An inexperienced player may risk burdening themselves with items they want to keep. This is risky play. An experienced player may create a stash knowing that shortly Sokoban will provide a solution to the problem of being overburdened.</p>

	<h3>The Structuralist Approach is Viable</h3>

	<p>So what is the point of this? I claim that experienced players intuit or explicitly understand all of this structure already. A meaningful play experience is created through a well-designed set of structures in a roguelike. If any of the layers of structure are poorly designed, this will create an uninteresting or unbalanced game.</p>

	<p>For example, if the first layer is poor, then the player may feel overwhelmed at the lack of logic in symbol choice or character structure. If the second layer is poor, the player may not come to any understanding of turn-to-turn strategy or experience any sense of exploration. If the highest level is poor, the game will lack a solid structure that ties it together. It will lack a narrative and sense of progression. Most importantly, it will lack high-level strategy.</p>

	<p>Well-designed structure allows the player to learn the aspects of the game that persist between playthroughs. Too much randomness provides the player with meaningless chaos. A successful roguelike lets the player uncover this structure without guidance, creating&nbsp;meaning for the player. As the player improves, each game becomes a way to exercise creativity. The specifics of the situation change from game to game, but the structure gives the player something to grasp onto and create a meaningful experience.</p>
{% endblock %}
{% block article_bio_content %}
	Matthew Ward regularly blogs at <a href="http://hilbertthm90.wordpress.com">"A Mind for Madness"</a>&nbsp;on&nbsp;math, philosophy, literature, and games.
{% endblock %}
{% block article_references_content %}
	{{ super() }}
	<ol start="1">
	<li><span>Bogost, Ian. "Comparative Video Game Criticism."</span> <span class="italicd">Games and Culture</span><span>&nbsp;1.1 (2006): 41-46.</span></li>
	<li><span>Barthes, Roland, and Lionel Duisit. "An Introduction to the Structural Analysis of Narrative."</span> <span class="italicd">New Literary History</span><span>&nbsp;6.2 (1975): 237.</span></li>
	<li><span>Johnson, Mark. "The Semiotics of the Roguelike."</span> <span class="italicd">pending publication.</span></li>
	</ol>
{% endblock %}
